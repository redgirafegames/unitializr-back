# README #

The project is deployed and can be used at : http://www.unitializr.com

[Frontend project](https://bitbucket.org/redgirafegames/unitializr-front/src)

### Unitializr ###

Unitializr is an Open Source initializr for Unity projects.

This backend allows to :
- Generate a ZIP for a given project configuration, automatically generating the packages manifest.json from
the project's data 
- Configure default project and file templates used as a referetial by the front-end
- Generate very light anonymous stats (counter) on project use

This project is unofficial, [Unity](https://unity.com/) has no official connection with this project.

### Technos ###

This is a Go project based on [GinGonic](https://github.com/gin-gonic/gin) framework and [Postgres](https://www.postgresql.org/) persistence.

### Setup up 

##### Environment Variables #####
- UNITIALIZR_MODE : Sets the GinGonic mode, should be set to "release" for production deployment. Default if not set : "debug".
- UNITIALIZR_ADMIN_USER : The admin user id. Default if not set : "admin".
- UNITIALIZR_ADMIN_PASSWORD : The admin user pass. Default if not set : "admin".
- UNITIALIZR_CORS_ALLOW_ORIGIN : The CORS origin allowed, typically your frontend url. Default if not set : *.
- PORT : The port the application will be served on. Default if not set : 8081.
- DATABASE_URL : Used to initialize the Postgres connection. Default if not set : "host=localhost port=5432 user=postgres dbname=Unitializr password=admin sslmode=disable" 

##### Authentication #####
Admin routes are protected by a basic auth using Admin user and password.

##### Referentials #####
Unity versions 
- Create a version : /admin/versions
- I currently haven't found clean way to get Unity versions packages data.
So the data are extracted in the most dirty (and incomplete) possible way by scraping the Unity's documentation.
- After creating a version, you need to initialize its data by calling the task : /admin/tasks/versions/:UNITY_VERSION/packages
- If you know a place or a way to get those data the right way, please reach out to me!

Default Templates
- You need to initialize default project and file templates by calling the REST API
- Projects : /admin/templates/projects
- Files : /admin/templates/files

### Contribution ###

I made this project mainly to learn Go and VueJs, therefore code can be far from perfect.
Contributions and remarks are welcomed. 

### Evolutions Roadmap ###
- TODO : generate a clean swagger file for the API 
- Enhance GetProject error notifications to the client
- User creation/authentication
- Remote saving project templates
- Remote saving file templates

### Contact ###

The project is developed and maintained by [RedGirafeGames](http://www.redgirafegames.com)
Contact : redgirafegames@gmail.com

### License ###
[MIT](https://choosealicense.com/licenses/mit/)