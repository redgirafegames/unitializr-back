package main

import (
	"github.com/gin-gonic/gin"
	"log"
	"os"
	"unitializr-back/middlewares"
	"unitializr-back/models"
)

func main() {
	log.Println("[Main] Unitializr starting...")

	mode := os.Getenv("UNITIALIZR_MODE")
	if mode == "" {
		mode = "debug"
	}

	log.Println("[Main] Mode : " + mode)
	gin.SetMode(mode)
	router := gin.Default()

	dbUrl := os.Getenv("DATABASE_URL")
	db := models.SetupModels(dbUrl)

	models.CheckDefaultAdminUser(db)
	adminUser := os.Getenv("UNITIALIZR_ADMIN_USER")
	if adminUser == ""{
		adminUser = "admin"
	}
	adminUserPass := os.Getenv("UNITIALIZR_ADMIN_PASSWORD")
	if adminUserPass == ""{
		adminUserPass = "admin"
	}
	accounts := gin.Accounts{
		adminUser: adminUserPass,
	}

	log.Println("Admin user added : " + adminUser)

	corsAllowOrigin := os.Getenv("UNITIALIZR_CORS_ALLOW_ORIGIN")
	if corsAllowOrigin == ""{
		corsAllowOrigin = "*"
	}
	log.Println("[Main] CORS Allowed origins : " + corsAllowOrigin)
	router.Use(middlewares.CORSMiddleware(corsAllowOrigin))

	router.Use(func(c *gin.Context) {
		c.Set("db", db)
		c.Next()
	})

	log.Println("[Main] Initializing REST routes")
	initializeRoutes(router, accounts)

	port := os.Getenv("PORT")
	if port == ""{
		port = "8081"
	}
	log.Println("[Main] Serving port : " + port)

	log.Println("[Main] Unitializr-back started")
	router.Run(":" + port)
}