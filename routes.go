package main

import (
	"github.com/gin-gonic/gin"
	"unitializr-back/controllers"
)

// initializeRoutes initialize all the REST resources access
// Admin resources are protected with basic auth
func initializeRoutes(router *gin.Engine, accounts gin.Accounts) {

	router.GET("/versions", controllers.GetVersions)

	router.GET("/templates/projects", controllers.GetProjectTemplates)

	router.GET("/templates/files", controllers.GetFileTemplates)

	router.POST("/projects", controllers.GetProjectZip)

	// admin resources
	adminRoutes := router.Group("/admin", gin.BasicAuth(accounts))
	{
		adminRoutes.POST("/versions", controllers.AddVersion)
		adminRoutes.DELETE("/versions/:id", controllers.DeleteVersion)
		adminRoutes.POST("/tasks/versions/:name/packages", controllers.UpdateVersionPackages)

		// App will need real auth and user handling to allow remote creation for anyone
		// Right now POST/DELETE of templates is only allowed for admin to create the default template list
		adminRoutes.POST("/templates/projects", controllers.AddProjectTemplate)
		adminRoutes.DELETE("/templates/projects/:id", controllers.DeleteProjectTemplate)
		adminRoutes.POST("/templates/files", controllers.AddFileTemplate)
		adminRoutes.DELETE("/templates/files/:id", controllers.DeleteFileTemplate)

		// Stats
		adminRoutes.GET("/stats/daily", controllers.GetDailyCountStats)
	}
}