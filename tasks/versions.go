package tasks

import (
	"errors"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/jinzhu/gorm"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"strings"
	"unitializr-back/models"
)

const versionManualPackagesBaseUrl = "https://docs.unity3d.com/%s/Documentation/Manual/pack-%s.html"
const packageMetadataBaseUrl = "https://docs.unity3d.com/Packages/metadata/%s/metadata.json"
const verifiedUrl = "safe"
const previewUrl = "preview"
const builtInUrl = "build"

// UpdateVersionPackages scraps Unity's documentation to find package's data (package id and version used by Unity version)
// It should definitely not be made this way.
// Some data are missing because the Unity documentation isn't the same for all packages.
// This task tries to cover as much cases as possible.
// Version must be created in DataBase before calling this task
func UpdateVersionPackages(db *gorm.DB, versionName string) ([]models.Package, error) {
	log.Printf("[tasks.versions] Updating version %s packages", versionName)

	var packages []models.Package

	var version models.UnityVersion
	if db.Preload("Packages").Where("name = ?", versionName).Find(&version).RecordNotFound() {
		return packages, errors.New(fmt.Sprintf("Version not found <%s>", versionName))
	}

	if len(version.Packages) > 0 {
		oldPackages := version.Packages
		version.Packages = make([]models.Package, 0)
		for _, p := range oldPackages {
			fmt.Printf("[tasks.versions] - <"+p.Name+"> <"+p.Value+"> <%v>\n", p.BaseModel.ID)
			db.Delete(&p)
		}
	}

	println("[tasks.versions] Extracting verified packages")
	verifiedPackages, err := extractPackagesFrom(fmt.Sprintf(versionManualPackagesBaseUrl, version.Name, verifiedUrl), models.PackageTypeVerified)
	if err != nil {
		return packages, err
	}
	packages = append(packages, verifiedPackages...)
	for _, p := range packages {
		//db.Create(&p)
		println("[tasks.versions] + <" + p.Name + "> <" + p.Value + ">")
	}

	println("[tasks.versions] Extracting preview packages")
	previewPackages, err := extractPackagesFrom(fmt.Sprintf(versionManualPackagesBaseUrl, version.Name, previewUrl), models.PackageTypePreview)
	if err != nil {
		return packages, err
	}
	packages = append(packages, previewPackages...)
	for _, p := range packages {
		//db.Create(&p)
		println("[tasks.versions] + <" + p.Name + "> <" + p.Value + ">")
	}

	println("[tasks.versions] Extracting built-in packages")
	builtInPackages, err := extractPackagesFrom(fmt.Sprintf(versionManualPackagesBaseUrl, version.Name, builtInUrl), models.PackageTypeBuiltIn)
	if err != nil {
		return packages, err
	}
	packages = append(packages, builtInPackages...)
	for _, p := range packages {
		//db.Create(&p)
		println("[tasks.versions] + <" + p.Name + "> <" + p.Value + ">")
	}
	log.Println(fmt.Sprintf("Number of new packages <%v>", len(packages)))

	log.Println("[tasks.versions] Saving Version new packages")
	version.Packages = packages
	if db.Save(&version).Error != nil {
		return packages, errors.New(fmt.Sprintf("Error saving version"))
	}

	log.Println("[tasks.versions] Update end.")

	return packages, nil
}

// extractPackagesFrom Extracts packages name and version from the Unity's documentation page
func extractPackagesFrom(packagesUrl string, packageType string) ([]models.Package, error) {
	log.Println("[tasks.version] Extracting packages url : " + packagesUrl)

	var packages []models.Package
	// Request the HTML page.
	res, err := http.Get(packagesUrl)
	if err != nil {
		return packages, errors.New(fmt.Sprintf("Error requesting url %s", packagesUrl))
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		return packages, errors.New(fmt.Sprintf("Error requesting url %s", packagesUrl))
	}

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		return packages, errors.New(fmt.Sprintf("Error loading html doc"))
	}

	// Find all list items
	doc.Find("li").Each(func(i int, s *goquery.Selection) {
		var packageLink = s.Find("a")
		var packageName = packageLink.Text()
		if len(packageLink.Nodes) > 0 && strings.Contains(packageName, "com.") {
			var packageVersion = ""
			href, _ := packageLink.Attr("href")
			var versionSeparatorSplit = strings.Split(href, "@")
			if len(versionSeparatorSplit) <= 1 {
				// built-in packages all have 1.0.0 version and some verified or preview packages are out of the standard
				// for documentation, making it hard to find the right version to use
				packageVersion = "1.0.0"
			}

			if packageVersion == "" {
				// Here we get the major and minor digits
				versionBase := strings.Split(versionSeparatorSplit[1], "/")[0]
				// We have to go in the package metadata to find the current patch digit
				patchDigit, err := extractPatchVersionFrom(fmt.Sprintf(packageMetadataBaseUrl, packageName), versionBase)
				if err != nil {
					log.Println(fmt.Sprintf("Error getting patch digit for %s", packageName), err)
					packageVersion = "0.0.0"
				} else {
					packageVersion = versionBase + "." + patchDigit
				}

			}

			newPackage := new(models.Package)
			newPackage.Name = packageName
			newPackage.Value = packageVersion
			newPackage.Type = packageType
			packages = append(packages, *newPackage)
		}
	})

	return packages, nil
}

// extractPatchVersionFrom The patch digit of package versionning is found using the metadata file of Unity's documentation
func extractPatchVersionFrom(metadataUrl string, versionBase string) (string, error) {
	var patchDigit = ""

	// Request the HTML page.
	res, err := http.Get(metadataUrl)
	if err != nil {
		return patchDigit, errors.New(fmt.Sprintf("Error requesting metadata url %s", metadataUrl))
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		return patchDigit, errors.New(fmt.Sprintf("Error metdata url returned code %d", res.StatusCode))
	}

	re := regexp.MustCompile(`"` + regexp.QuoteMeta(versionBase) + `.+?"`)
	metadataContent, _ := ioutil.ReadAll(res.Body)
	fullVersions := re.FindAllString(string(metadataContent), -1)
	if len(fullVersions) == 0 {
		return patchDigit, errors.New(fmt.Sprintf("Could not find complete version for base %s", versionBase))
	}

	var maxPatchDigit = "0"
	for _, vers := range fullVersions {
		patch := strings.ReplaceAll(strings.ReplaceAll(vers, versionBase + ".", ""), "\"", "")
		if strings.Compare(patch, maxPatchDigit) > 0{
			maxPatchDigit = patch
		}
	}
	return maxPatchDigit, nil
}
