package models

// Basic text file
const FileItemTypeText = "TEXT"
// Manifest file, its content is generated from the project's packages configuration
const FileItemTypeManifest = "MANIFEST"
// Editor Version file, its content is generated from the project's Unity version
const FileItemTypeEditorVersion = "EDITORVERSION"
// Url fil, its content is populated from the URL content. Mainly used with Git servers raw urls
const FileItemTypeUrl = "URL"
// Directory
const FileItemTypeDir = "DIR"

type FileItem struct {
	BaseModel
	Name   string `json:"name"`
	Locked bool   `json:"locked"`
	// Enum should be more elegant, but I don't want clients to deal with integers in json
	Type           string     `json:"type"`
	Value          string     `json:"value"`
	ProjectID      uint       `json:"-"`
	FileTemplateID uint       `json:"-"`
	FileItemID     uint       `json:"-"`
	Children       []FileItem `json:"children"`
}

// GetFileItemTypes returns all file types
func GetFileItemTypes() []string {
	typesArr := []string{FileItemTypeText, FileItemTypeManifest, FileItemTypeUrl, FileItemTypeEditorVersion, FileItemTypeDir}
	return typesArr
}

// IsValidFileItemType returns if the given type is a valid file type
func IsValidFileItemType(t string) bool {
	for _, s := range GetPackageTypes() {
		if s == t {
			return true
		}
	}
	return false
}
