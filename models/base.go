package models

import "time"

type BaseModel struct {
	ID        uint       `gorm:"primary_key"`
	CreatedAt time.Time  `json:"-"`
	UpdatedAt time.Time  `json:"-"`
	// removing soft delete as number of lines are limited on heroku base plan ( :/ )
	//DeletedAt *time.Time `json:"-";sql:"index"`
}
