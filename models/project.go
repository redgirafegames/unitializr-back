package models

type Project struct {
	BaseModel
	Name          string     `json:"name"`
	VersionName   string     `json:"versionName"`
	VersionSuffix string     `json:"versionSuffix"`
	Files         []FileItem `json:"files"`
	Packages      []Package  `json:"packages" gorm:"association_autoupdate:false`
}
