package models

type ProjectTemplate struct {
	BaseModel
	Name        string  `json:"name"`
	Description string  `json:"description"`
	Default     bool    `json:"default"`
	Visibility  string  `json:"visibility"`
	User        User    `json:"user" gorm:"association_autoupdate:false;association_autocreate:false"`
	UserID      uint    `json:"-"`
	Project     Project `json:"project"`
	ProjectID   uint    `json:"-"`
}

type FileTemplate struct {
	BaseModel
	Name        string   `json:"name"`
	Description string   `json:"description"`
	Visibility  string   `json:"visibility"`
	User        User     `json:"user" gorm:"association_autoupdate:false;association_autocreate:false"`
	UserID      uint     `json:"-"`
	File        FileItem `json:"file"`
}
