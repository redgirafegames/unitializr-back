package controllers

import (
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"net/http"
	"unitializr-back/models"
)

// GetDailyCountStats Returns all daily count stats.
// Daily count stats persist the number of action call date
func GetDailyCountStats(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	var dailyStats []models.DailyCountStat
	db.Find(&dailyStats)

	c.JSON(http.StatusOK, dailyStats)
}

// IncrementDailyCountStat Increment the action count for the date
func IncrementDailyCountStat(c *gin.Context, day int, month int, year int, action string) {
	db := c.MustGet("db").(*gorm.DB)

	var stat models.DailyCountStat
	stat.Day = day
	stat.Month = month
	stat.Year = year
	stat.Action = action

	db.Where(&stat).FirstOrCreate(&stat, models.DailyCountStat{Day: day, Month: month, Year: year, Action: action})

	stat.Count++

	db.Save(&stat)

	c.JSON(http.StatusOK, stat)
}