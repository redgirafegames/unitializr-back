package models

type UnityVersion struct {
	BaseModel
	Name string `json:"name" gorm:"primary_key"`
	Suffix string `json:"suffix"`
	Packages []Package `json:"packages"`
	Deprecated bool `json:"deprecated"`
	Alpha bool `json:"alpha"`
	Lts bool `json:"lts"`
}