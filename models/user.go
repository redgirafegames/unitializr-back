package models

import (
	"github.com/jinzhu/gorm"
	"log"
)

type User struct {
	BaseModel
	Name       string `json:"name"`
	Admin      bool   `json:"admin"`
}

// CheckDefaultAdminUser Check that an admin user exists and if not, create it
// Made this way until a real user management system is created
func CheckDefaultAdminUser(db *gorm.DB) {

	var adminUser User
	if db.Where("admin = ?", true).First(&adminUser).RecordNotFound() {
		var defaultAdminUser = new(User)
		defaultAdminUser.Name = "Admin"
		defaultAdminUser.Admin = true
		db.Create(defaultAdminUser)

		log.Println("[User] Created default Admin user in db")
	}

}
