package models

const StatTypeGetProject = "GETPROJECT"

type DailyCountStat struct {
	BaseModel
	Day    int    `json:"day" gorm:"primaryKey;autoIncrement:false"`
	Month  int    `json:"month" gorm:"primaryKey;autoIncrement:false"`
	Year   int    `json:"year" gorm:"primaryKey;autoIncrement:false"`
	Action string `json:"action" gorm:"primaryKey;autoIncrement:false"`
	Count  int    `json:"count" gorm:"default:0"`
}

// GetStatTypes returns all stat types
func GetStatTypes() []string {
	typesArr := []string{StatTypeGetProject}
	return typesArr
}

// IsValidStatType returns if the given stat type exists
func IsValidStatType(t string) bool {
	for _, s := range GetStatTypes() {
		if s == t {
			return true
		}
	}
	return false
}
