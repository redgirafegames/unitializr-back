package models

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

const AutoConfigUrl = "host=localhost port=5432 user=postgres dbname=Unitializr password=admin sslmode=disable"

// SetupModels Setup the postgres database
func SetupModels(dbUrl string) *gorm.DB {

	if dbUrl != "" {
		println("Trying to open database postgres for : ")
		println(dbUrl)
		db, err := gorm.Open("postgres", dbUrl)
		if err != nil {
			panic("Failed to connect to database! : " + err.Error())
		}

		AutoMigrateTables(db)

		return db
	} else {
		println("Trying to open auto config localhost database:5432")
		db, err := gorm.Open("postgres", AutoConfigUrl)
		if err != nil {
			panic("Failed to connect to database! : " + err.Error())
		}

		AutoMigrateTables(db)

		return db
	}
}

func AutoMigrateTables(db *gorm.DB) {
	db.AutoMigrate(&User{})
	db.AutoMigrate(&ProjectTemplate{})
	db.AutoMigrate(&FileTemplate{})
	db.AutoMigrate(&Project{})
	db.AutoMigrate(&Package{})
	db.AutoMigrate(&FileItem{})
	db.AutoMigrate(&UnityVersion{})
	db.AutoMigrate(&DailyCountStat{})
}