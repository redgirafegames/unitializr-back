package controllers

import (
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"net/http"
	"unitializr-back/models"
	"unitializr-back/tasks"
)

// GetVersions Return all Unity versions
func GetVersions(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	var versions []models.UnityVersion
	db.Set("gorm:auto_preload", true).Find(&versions)

	c.JSON(http.StatusOK, versions)
}

// AddVersion Create a new version. UpdateVersionPackages still has to be used to update version's packages
func AddVersion(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)


	var version models.UnityVersion
	if err := c.ShouldBindJSON(&version); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"Error unity version invalid : ": err.Error()})
		return
	}

	res := db.Create(&version)
	c.JSON(http.StatusOK, res)
}

// UpdateVersionPackages Updates verified, preview and built-in packages lists of the Unity version
func UpdateVersionPackages(c *gin.Context){
	db := c.MustGet("db").(*gorm.DB)

	packages, err := tasks.UpdateVersionPackages(db, c.Param("name"))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"Error during task execution : ": err.Error()})
		return
	}

	c.JSON(http.StatusOK, packages)

}

// DeleteVersion Delete the version by id
func DeleteVersion(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)

	var version models.UnityVersion
	if err := db.Where("ID = ?", c.Param("id")).First(&version).Error; err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Version not found"})
		return
	}

	res := db.Delete(&version)

	c.JSON(http.StatusOK, res)
}
