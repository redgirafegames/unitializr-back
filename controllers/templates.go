package controllers

import (
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"net/http"
	"unitializr-back/models"
)

// GetProjectTemplates returns all public project templates.
// When a user management system will be in place, it should also return private templates of the authenticated user
func GetProjectTemplates(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)

	var templates []models.ProjectTemplate
	db.Set("gorm:auto_preload", true).Find(&templates, "visibility = 'public'")

	c.JSON(http.StatusOK, templates)
}

// AddProjectTemplate Add a new Project template
func AddProjectTemplate(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	var template models.ProjectTemplate
	if err := c.ShouldBindJSON(&template); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"Error project template invalid : ": err.Error()})
		return
	}

	// Add ProjectTemplate is only allowed for admin user right now
	var adminUser models.User
	db.Where("admin = ?", true).First(&adminUser)
	template.User = adminUser

	db.Create(&template)

	db.Save(&template)

	c.JSON(http.StatusOK, template)
}

// DeleteProjectTemplate Delete project template by id
func DeleteProjectTemplate(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)

	var template models.ProjectTemplate
	if err := db.Where("ID = ?", c.Param("id")).First(&template).Error; err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "ProjectTemplate not found"})
		return
	}

	res := db.Delete(&template)

	c.JSON(http.StatusOK, res)
}


// GetFileTemplates returns all public file templates
// When a user management system will be in place, it should also return private templates of the authenticated user
func GetFileTemplates(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)

	var templates []models.FileTemplate
	// TODO : add select filter on user when auth is implemented
	db.Set("gorm:auto_preload", true).Find(&templates, "visibility = 'public'")

	c.JSON(http.StatusOK, templates)
}

// AddFileTemplate Add a new File Template
func AddFileTemplate(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	var template models.FileTemplate
	if err := c.ShouldBindJSON(&template); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"Error file template invalid : ": err.Error()})
		return
	}

	// Add FileTemplate is only allowed for admin user right now
	var adminUser models.User
	db.Where("admin = ?", true).First(&adminUser)
	template.User = adminUser

	db.Create(&template)

	db.Save(&template)

	c.JSON(http.StatusOK, template)
}

// DeleteFileTemplate Delete File Template by id
func DeleteFileTemplate(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)

	var template models.FileTemplate
	if err := db.Where("ID = ?", c.Param("id")).First(&template).Error; err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "FileTemplate not found"})
		return
	}

	res := db.Delete(&template)

	c.JSON(http.StatusOK, res)
}
