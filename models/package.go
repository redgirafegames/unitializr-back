package models

const PackageTypeVerified = "verified"
const PackageTypeBuiltIn = "built-in"
const PackageTypePreview = "preview"
const PackageTypeCustom = "custom"

type Package struct {
	BaseModel `json="-"`
	Name  string `json:"name"`
	Type  string `json:"type"`
	Value string `json:"value"`
	ProjectID uint  `json:"-"`
	UnityVersionID uint `json:"-"`
}

// GetPackageTypes returns all package types
func GetPackageTypes()[]string{
	typesArr := []string{PackageTypeVerified, PackageTypeBuiltIn, PackageTypePreview, PackageTypeCustom}
	return typesArr
}

// IsValidPackageType returns if the given type is a valid package type
func IsValidPackageType(t string)bool{
	for _, s := range GetPackageTypes() {
		if s == t {
			return true
		}
	}
	return false
}
