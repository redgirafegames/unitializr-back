package controllers

import (
	"archive/zip"
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"
	"unitializr-back/models"
)

const ManifestIndentToken = "  "

// GetProjectZip Returns the input project as a Zip file
// All file contents are supposed to be encoded in base64.
func GetProjectZip(c *gin.Context) {

	initZipResultHeaders(c)

	inputProject, _ := getProjectInput(c)

	log.Println("Creating project zip...")
	zipWriter := zip.NewWriter(c.Writer)

	addFilesToZip(inputProject.Files, "", inputProject, zipWriter)

	zipWriter.Close()
	log.Println("Project zip created")

	// Incrementing stats
	d := time.Now()
	year, month, day := d.Date()
	IncrementDailyCountStat(c, day, int(month), year, models.StatTypeGetProject)

}

// addFilesToZip recursively add all files to the zip
func addFilesToZip(files []models.FileItem, parentPath string, project *models.Project, writer *zip.Writer) {
	if len(files) == 0 {
		return
	}
	for i := 0; i < len(files); i++ {
		projectFile := files[i]
		filePath := parentPath + projectFile.Name
		if !strings.EqualFold(projectFile.Type, models.FileItemTypeDir) {

			addFileToZip(&projectFile, filePath, project, writer)

		} else {
			dirPath := filePath + "/"
			log.Println("Add dir : " + dirPath)
			writer.Create(dirPath)
			addFilesToZip(projectFile.Children, dirPath, project, writer)
		}
	}
}

// addFileToZip add a file to the the zip handling its specific type and decoding from base64 its value.oject, writer *zip.Writer) {
func addFileToZip(file *models.FileItem, path string, project *models.Project, writer *zip.Writer) {
	log.Println("Add file : " + path)
	zipWriterFile, _ := writer.Create(path)

	fileContent := ""

	decodedValue, err := base64.StdEncoding.DecodeString(file.Value)
	if err != nil {
		log.Println("Could not decode the file value (that should be encoded in base64) <" + file.Value + ">", err)
		fileContent = "UNITIALIZR ERROR : Could not decode the file value (that should be encoded in base64) <" + file.Value + ">"
	} else {
		fileContent = getFileContent(file, decodedValue, project)
	}

	io.Copy(zipWriterFile, strings.NewReader(fileContent))
}

// getFileContent Returns the content of the file using its type
// TEXT : the content is the file value
// URL : the content is requested at the url defined in the file value
// MANIFEST : the content is generated from the packages selection
func getFileContent(file *models.FileItem, fileValue []byte, project *models.Project) string {
	if strings.EqualFold(file.Type, models.FileItemTypeText) {
		return string(fileValue)
	} else if strings.EqualFold(file.Type, models.FileItemTypeUrl) {

		url := string(fileValue)
		if strings.EqualFold(url, "") {
			return ""
		}
		res, err := http.Get(url)
		if err != nil {
			log.Println("File content could not be retrieved from url <"+url+">", err)
			return "UNITIALIZR ERROR : file content could not be retrieved from url <" + url + ">"
		}
		defer res.Body.Close()
		if res.StatusCode != 200 {
			log.Println("File content could not be retrieved from url <"+url+">. Accessing url returned code <"+fmt.Sprintf("%d", res.StatusCode)+">.", err)
			return "UNITIALIZR ERROR : File content could not be retrieved from url <" + url + ">. Accessing url returned code <" + fmt.Sprintf("%d", res.StatusCode) + ">."
		}

		html, err := ioutil.ReadAll(res.Body)
		if err != nil {
			log.Println("File content from url <"+url+"> could not be parsed.", err)
			return "UNITIALIZR ERROR : File content from url <" + url + "> could not be parsed."
		}

		return string(html)
	} else if strings.EqualFold(file.Type, models.FileItemTypeManifest) {
		return generateManifest(project)
	} else if strings.EqualFold(file.Type, models.FileItemTypeEditorVersion) {
		return generateEditorVersion(project)
	}
	return string(fileValue)
}

// generateManifest Generate unity packages manifest using project's configuration
func generateManifest(project *models.Project) string {
	packages := project.Packages

	var manifest = "{"

	// dependencies
	manifest += "\"dependencies\":{"
	for i, pkg := range packages {
		manifest += "\"" + pkg.Name + "\" : \"" + pkg.Value + "\""
		if i != len(packages) - 1 {
			manifest += ","
		}
	}
	// end dependencies
	manifest += "}"

	// end manifest
	manifest += "}"

	var manifestPrettyPrint bytes.Buffer
	error := json.Indent(&manifestPrettyPrint, []byte(manifest), "", ManifestIndentToken)
	if error != nil {
		log.Println("JSON parse error: ", error)
		return "UNITIALIZR ERROR : Could not generate valid json manifest"
	}
	return string(manifestPrettyPrint.Bytes())
}

// generateEditorVersion generate unity editor version file
func generateEditorVersion(project *models.Project) string {
	version := "m_EditorVersion: " + project.VersionName
	suffix := "." + project.VersionSuffix
	if len(suffix) > 1 {
		version += suffix
	}
	return version
}


// getProjectInput return project from request POST body
func getProjectInput(c *gin.Context) (*models.Project, error) {
	// Validate input
	var input models.Project
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return nil, err
	}
	return &input, nil
}

// initZipResultHeaders set the right headers to return a zip file
func initZipResultHeaders(c *gin.Context) {
	c.Writer.Header().Set("Content-type", "application/octet-stream")
	c.Writer.Header().Set("Content-Disposition", "attachment; filename='filename.zip'")
}
